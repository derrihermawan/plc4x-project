package openplc;


import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

import org.apache.plc4x.java.PlcDriverManager;
import org.apache.plc4x.java.api.PlcConnection;
import org.apache.plc4x.java.api.messages.PlcReadRequest;
import org.apache.plc4x.java.api.messages.PlcReadResponse;
import org.apache.plc4x.java.api.messages.PlcWriteRequest;
import org.apache.plc4x.java.api.messages.PlcWriteResponse;
import org.apache.plc4x.java.api.types.PlcResponseCode;
public class App {

	public static void main(String[] args) {
		String connection = "modbus:tcp://localhost:512?slaveId=1";
		//String connection = "modbus://localhost";		
		
		try {
			PlcConnection plcConnection = new PlcDriverManager().getConnection(connection);
			System.out.println("sukses");
			// Check if this connection support reading of data.
			if (!plcConnection.getMetadata().canWrite()) {
				System.out.println("This connection doesn't support writing.");
			  return;
			}
			// Create a new read request:
			// - Give the single item requested the alias name "value"
			PlcWriteRequest.Builder builderWrite = plcConnection.writeRequestBuilder();
			builderWrite.addItem("value-1", "coil:1[2]", false, false);
			builderWrite.addItem("value-2", "coil:3", true);
//			builderWrite.addItem("value-3", "holding-register:1", 42);
//			builderWrite.addItem("value-4", "holding-register:3[4]", 1, 2, 3, 4);
			PlcWriteRequest writeRequest = builderWrite.build();
			
			PlcWriteResponse responseWrite = writeRequest.execute().get();
			for (String fieldName : responseWrite.getFieldNames()) {
			    if(responseWrite.getResponseCode(fieldName) == PlcResponseCode.OK) {
			        System.out.println("Value Write[" + fieldName + "]: successfully written to device.");
			    }
			    // Something went wrong, to output an error message instead.
			    else {
			    	System.out.println("Error[" + fieldName + "]: " + responseWrite.getResponseCode(fieldName).name());
			    }
			}
			
			
			if(!plcConnection.getMetadata().canRead()) {
				System.out.println("Cant read data");
				return;
			}
			
			// Create a new read request:
			// - Give the single item requested the alias name "value"
			PlcReadRequest.Builder builder = plcConnection.readRequestBuilder();
			builder.addItem("value-1", "coil:1[2]");
			builder.addItem("value-2", "coil:3");
//			builder.addItem("value-3", "holding-register:1");
//			builder.addItem("value-4", "holding-register:3[2]");
			PlcReadRequest readRequest = builder.build();
			
			
			PlcReadResponse response = readRequest.execute().get();
			for (String fieldName : response.getFieldNames()) {
			    if(response.getResponseCode(fieldName) == PlcResponseCode.OK) {
			        int numValues = response.getNumberOfValues(fieldName);
			        // If it's just one element, output just one single line.
			        if(numValues == 1) {
			            System.out.println("Value[" + fieldName + "]: " + response.getObject(fieldName));
			        }
			        // If it's more than one element, output each in a single row.
			        else {
			            System.out.println("Value[" + fieldName + "]:");
			            for(int i = 0; i < numValues; i++) {
			                System.out.println(" - " + response.getObject(fieldName, i));
			            }
			        }
			    }
			    // Something went wrong, to output an error message instead.
			    else {
			        System.out.println("Error[" + fieldName + "]: " + response.getResponseCode(fieldName).name());
			    }
			}
			
		}
		catch(Exception e) {
			System.out.print("Message : " + e);
		}

	}

}
